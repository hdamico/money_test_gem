# lib/money.rb
require "money_hdamico"

class Money

  attr_accessor :currency, :amount, :conversions

  def initialize(amount = 0, currency = 'EUR')
    @currency = currency
    @amount = amount
    @conversions = {
      'USD' => 1.11,
      'Bitcoin' => 0.0047
    }
  end

  def convert_to(currency)
    return raise 'Incorrect currency' unless self.currency != currency || @conversions.include?(currency)

    self.amount = (@conversions[currency] * amount).round(2)
    self.currency = currency
    self
  end

  def -(other)
    if other.is_a?(MoneyHdamico)
      other.convert_to(currency) if other.currency != currency
      MoneyHdamico.new(amount - other.amount, currency)
    elsif other.is_a?(Numeric)
      MoneyHdamico.new(amount - other, currency)
    else
      raise 'Cannot convert String into Numeric'
    end
  end

  def +(other)
    if other.is_a?(MoneyHdamico)
      other.convert_to(currency) if other.currency != currency
      MoneyHdamico.new(amount + other.amount, currency)
    elsif other.is_a?(Numeric)
      MoneyHdamico.new(amount + other, currency)
    else
      raise 'Cannot convert String into Numeric'
    end
  end

  def /(other)
    if other.is_a?(MoneyHdamico)
      other.convert_to(currency) if other.currency != currency
      MoneyHdamico.new(amount / other.amount, currency)
    elsif other.is_a?(Numeric)
      MoneyHdamico.new(amount / other, currency)
    else
      raise 'Cannot convert String into Numeric'
    end
  end

  def *(other)
    if other.is_a?(MoneyHdamico)
      other.convert_to(currency) if other.currency != currency
      MoneyHdamico.new(amount * other.amount, currency)
    elsif other.is_a?(Numeric)
      MoneyHdamico.new(amount * other, currency)
    else
      raise 'Cannot convert String into Numeric'
    end
  end

  def <(other)
    if other.is_a?(MoneyHdamico)
      other.convert_to(currency) if other.currency != currency
      return true if amount < other.amount
    end
    false
  end

  def >(other)
    if other.is_a?(MoneyHdamico)
      other.convert_to(currency) if other.currency != currency
      return true if amount > other.amount
    end
    false
  end

  def ==(other)
    if other.is_a?(MoneyHdamico)
      return true if amount == other.amount && currency == other.currency
    end
    false
  end
end
