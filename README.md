# MoneyHdamico

This is a interview exercise, where i had to create a Money gem.

## Installation
```ruby
gem 'money_hdamico'
```
```ruby
gem install 'money_hdamico'
```

## Usage

* Create a new object using Money.new (default values are: amount 0 and currency 'EUR').
* Can convert to 'USD' using .convert_to (available currency is only 'USD').
* Can do arithmetic operations (Money with Numeric and Money with Money).

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/hdamico/money_test_gem.
